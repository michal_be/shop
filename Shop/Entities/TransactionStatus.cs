﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Entities
{
    public enum TransactionStatus
    {
        Chart = 0,
        Finalized = 1
    }

}