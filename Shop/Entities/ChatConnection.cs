﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shop.Entities
{
    public class ChatConnection
    {
        [Key]
        public int ConnectionId { get; set; }
        public string FirstUserId { get; set; }
        public string SecondUserId { get; set; }
        public ICollection<ChatMessage> ChatMessages { get; set; } 
    }
}