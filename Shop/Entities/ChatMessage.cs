﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Shop.Entities
{
    public class ChatMessage
    {
        [Key]
        public int Id { get; set; }
        public int ConnectionId { get; set; }
        public ChatConnection ChatConnection { get; set; }
        public string MessageAuthorId { get; set; }
        public string MessageContent { get; set; }
        public DateTime MessageDate { get; set; }
    }
}