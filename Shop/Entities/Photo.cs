﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shop.Entities
{
    public class Photo
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }

        [ForeignKey("ItemId")]
        public virtual Item Item { get; set; }
        public long ItemId { get; set; }

    }
}