﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.EnterpriseServices;
using System.Linq;
using System.Web;

namespace Shop.Entities
{
    public class Transaction
    {   
        [Key]
        public long Id { get; set; }
        public long ItemId { get; set; }
        public string BuyerId { get; set; }

        [ForeignKey("ItemId")]
        public virtual Item BoughtItem { get; set; }
        public TransactionStatus Status { get; set; }
    }
}