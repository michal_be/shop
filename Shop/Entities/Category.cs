﻿using System.ComponentModel.DataAnnotations;

namespace Shop.Entities
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsDisabled { get; set; }

    }
}