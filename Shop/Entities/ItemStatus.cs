﻿using System;

namespace Shop.Entities
{
    public enum ItemStatus
    {
        Active = 0,
        Bought = 1,
        Blocked = 2
    }


    public static class EnumExtension
    {
        public static string ToDescription(this ItemStatus status)
        {

            string description = String.Empty;
            switch (status)
            {
                case ItemStatus.Active:
                    description = Resources.ResourcesPolish.ItemStatus_Active;
                    break;
                case ItemStatus.Bought:
                    description = Resources.ResourcesPolish.ItemStatus_Bought;
                    break;
                case ItemStatus.Blocked:
                    description = Resources.ResourcesPolish.ItemStatus_Blocked;
                    break;
            }

            return description;
        }
    }

}