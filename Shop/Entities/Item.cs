﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Shop.Models;

namespace Shop.Entities
{
    public class Item
    {
        [Key]
        public long Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }
 
        public Currency Currency { get; set; }
   
        public bool Negotiable { get; set; }

        public virtual List<Photo> Photos { get; set; }
        public Category Category { get; set; }

        [Required]
        public byte CategoryId { get; set; }

        [Required]
        public string OwnerId { get; set; }
    
        public ApplicationUser Owner { get; set; }

        public ItemStatus Status { get; set; }
    }
}