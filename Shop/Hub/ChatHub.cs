﻿using System.Web;
using Microsoft.AspNet.Identity;
using Shop.Services;

namespace Shop.Hub
{
    public class ChatHub: Microsoft.AspNet.SignalR.Hub
    {
        private readonly IChatService _chatService;

        public ChatHub(IChatService chatService)
        {
            _chatService = chatService;
        }

        public void SendMessage(string message, string destUserId, int connectionId)
        {
            Clients.User(destUserId).receiveMessage(message, destUserId);
            string authorMessageId = HttpContext.Current.User.Identity.GetUserId();
            _chatService.SaveMessageToDb(message, connectionId, authorMessageId);
        }


    }
}