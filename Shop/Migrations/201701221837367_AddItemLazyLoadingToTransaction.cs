namespace Shop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddItemLazyLoadingToTransaction : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Transactions", "ItemId");
            AddForeignKey("dbo.Transactions", "ItemId", "dbo.Items", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "ItemId", "dbo.Items");
            DropIndex("dbo.Transactions", new[] { "ItemId" });
        }
    }
}
