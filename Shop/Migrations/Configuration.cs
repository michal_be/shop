using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Shop.Entities;
using Shop.Models;

namespace Shop.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Shop.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Shop.Models.ApplicationDbContext context)
        {

            if (!context.Users.Any(u => u.Login == "michal123"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "michal123", Email = "michal.budzan@wp.pl", Name = "Micha�", Surname = "Budzan", BirthDate = new DateTime(1993, 9, 30), Login = "michal123" };
                manager.Create(user, "Michal123!");
            }

            if (!context.Categories.Any())
            {
                List<Category> categoriesList = new List<Category>()
                {
                    new Category() {Name = "Elektronika", IsDisabled = false},
                    new Category() {Name = "Moda i uroda", IsDisabled = false},
                    new Category() {Name = "Dom i zdrowie", IsDisabled = false},
                    new Category() {Name = "Dziecko", IsDisabled = false},
                    new Category() {Name = "Kultura i rozrywka", IsDisabled = false},
                    new Category() {Name = "Sport i wypoczynek", IsDisabled = false},
                    new Category() {Name = "Motoryzacja", IsDisabled = false},
                    new Category() {Name = "Kolekcje i sztuka", IsDisabled = false},
                    new Category() {Name = "Firma", IsDisabled = false},
                    new Category() {Name = "Inne", IsDisabled = false}
                };

                foreach (var category in categoriesList)
                {
                    context.Categories.Add(category);
                    context.SaveChanges();
                }
            }         
        }
    }
}
