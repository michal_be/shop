namespace Shop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ItemEntityWithotPhotoRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Items", "Photo1", c => c.Binary());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Items", "Photo1", c => c.Binary(nullable: false));
        }
    }
}
