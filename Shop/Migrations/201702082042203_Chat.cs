namespace Shop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Chat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChatConnections",
                c => new
                    {
                        ConnectionId = c.Int(nullable: false, identity: true),
                        FirstUserId = c.String(),
                        SecondUserId = c.String(),
                    })
                .PrimaryKey(t => t.ConnectionId);
            
            CreateTable(
                "dbo.ChatMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConnectionId = c.Int(nullable: false),
                        MessageAuthorId = c.String(),
                        MessageContent = c.String(),
                        MessageDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChatConnections", t => t.ConnectionId, cascadeDelete: true)
                .Index(t => t.ConnectionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChatMessages", "ConnectionId", "dbo.ChatConnections");
            DropIndex("dbo.ChatMessages", new[] { "ConnectionId" });
            DropTable("dbo.ChatMessages");
            DropTable("dbo.ChatConnections");
        }
    }
}
