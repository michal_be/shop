namespace Shop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhotoEntity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Items", new[] { "CategoryId" });
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        ItemId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.ItemId);
            
            AddColumn("dbo.Items", "Category_Id", c => c.Int());
            AlterColumn("dbo.Items", "CategoryId", c => c.Byte(nullable: false));
            CreateIndex("dbo.Items", "Category_Id");
            AddForeignKey("dbo.Items", "Category_Id", "dbo.Categories", "Id");
            DropColumn("dbo.Items", "Photo1");
            DropColumn("dbo.Items", "Photo2");
            DropColumn("dbo.Items", "Photo3");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "Photo3", c => c.Binary());
            AddColumn("dbo.Items", "Photo2", c => c.Binary());
            AddColumn("dbo.Items", "Photo1", c => c.Binary());
            DropForeignKey("dbo.Items", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.Photos", "ItemId", "dbo.Items");
            DropIndex("dbo.Photos", new[] { "ItemId" });
            DropIndex("dbo.Items", new[] { "Category_Id" });
            AlterColumn("dbo.Items", "CategoryId", c => c.Int(nullable: false));
            DropColumn("dbo.Items", "Category_Id");
            DropTable("dbo.Photos");
            CreateIndex("dbo.Items", "CategoryId");
            AddForeignKey("dbo.Items", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
        }
    }
}
