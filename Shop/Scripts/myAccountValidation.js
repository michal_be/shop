﻿$.validator.setDefaults({ ignore: null });

$(function () {
    $('#date').combodate({
        minYear: 1900,
        maxYear: 2017,
        customClass: 'btn',
        value: reversedDate
    });
});

var emailTaken;

function validateMyAccountForm() {
    
    $('#emailValidation').empty();
    var dateString = $('#date').combodate('getValue');
    document.getElementById('BirthDate').value = dateString;

    var email = $('#emailVal').val();
    checkIfEmailExists(email);

    if (!emailTaken) {
        $('#manageAccountForm').submit();
    }
}

function returnEmailStatus(emailExists) {
    if (emailExists) {
        $('#emailValidation').append('Podany adres jest zajęty');
        emailTaken = true;
    } else {
        emailTaken = false;
    }
}


function checkIfEmailExists(email) {

    $.ajax({
        method: "GET",
        url: "/api/account/checkIfEmailExists",
        data: { email: email },
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (result) {
            var emailExists = result;
            returnEmailStatus(emailExists);
        }
    });
}



