﻿function MainPageViewModel() {
    var that = this;
    var loadDataTimeout = 2000;
    this.dataUrl = "/api/mainPage";
    this.failedLoadingDataMessage = "";
    this.mainPage = ko.observable();
    var counter = 0;
    var index;
    var mainPageInfo;

    function getData() {
        $.get(that.dataUrl, successLoadData);
    }

    function successLoadData(data, status) {
        if (status === "success") {
            index = counter % 4;

            switch (index) {
                case 0:
                    mainPageInfo = {
                        pageInfo: "Ilość zarejestrowanych użytkowników: " + (data.AmountOfUsers)
                    };
                    break;
                case 1:
                    mainPageInfo = {
                        pageInfo: "Ilość aktywnych ofert: " + (data.AmountOfActiveOffers)
                    };
                    break;
                case 2:
                    mainPageInfo = {
                        pageInfo: "Ilość sfinalizowanych aukcji: " + (data.AmountOfFinalizedOffers)
                    };
                    break;
                case 3:
                    mainPageInfo = {
                        pageInfo: "Najlepszy sklep internetowy w Polsce"
                    };
                    break;
                default:
            }
                ++counter;             
                that.mainPage(mainPageInfo);
                $('.header').fadeIn(300);
            }
    }

    function reloadMainPage() {
        $('.header').fadeOut(300, getData);
    }

    getData();

    setInterval(reloadMainPage, loadDataTimeout);
}

var mainPageViewModel = new MainPageViewModel();
ko.applyBindings(mainPageViewModel, document.getElementById('content'));