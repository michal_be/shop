﻿$.validator.setDefaults({ ignore: null });

        $(function () {
            $('#date').combodate({
                minYear: 1900,
                maxYear: 2017,
                customClass: 'btn'

            });
        });

        var emailTaken;
        var loginTaken;

        function validateRegisterForm() {

            $('#emailValidation').empty();
            $('#loginValidation').empty();
            var dateString = $('#date').combodate('getValue');
            document.getElementById('BirthDate').value = dateString;

            var email = $('#emailVal').val();
            var login = $('#loginVal').val();

            checkIfEmailExists(email);
            checkIfLoginExists(login);

            if (!(emailTaken || loginTaken) ) {
                $('#registerForm').submit();
            }
        }

        function returnEmailStatus(emailExists) {
            if (emailExists) {
                $('#emailValidation').append('Podany adres jest zajęty');
                emailTaken = true;
            } else {
                emailTaken = false;
            }
        }


        function returnLoginStatus(loginExists) {
            if (loginExists) {
                loginTaken = true;
                $('#loginValidation').append('Podany login już istnieje');
            } else {
                loginTaken = false;
            }
        }


        function checkIfEmailExists(email) {

            $.ajax({
                method: "GET",
                url: "/api/account/checkIfEmailExists",
                data: { email: email },
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function(result) {
                    var emailExists = result;                
                    returnEmailStatus(emailExists);
                }
            });
        }


        function checkIfLoginExists(login) {

            $.ajax({
                method: "GET",
                url: "/api/account/checkIfLoginExists",
                data: { login: login },
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function(result) {
                    var loginExists = result;              
                    returnLoginStatus(loginExists);
                }
            });
        }