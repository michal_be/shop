﻿using Shop.Dtos;
using Shop.Entities;
using Shop.Models;
using System.Collections.Generic;

namespace Shop.Infrastructure
{
    public interface IMappingInfrastructure
    {
        Item MapItemFormDtoToItem(ItemFormDto itemFormDto);
        ManageAccountViewModel MapApplicationUserToManageAccountViewModel(ApplicationUser user);
        ApplicationUser MapManageAccountViewModelToApplicationUser(ManageAccountViewModel manageAccountViewModel);
        List<ShowItemViewModel> MapItemsToShowItemViewModels(List<Item> items);
        ShowItemViewModel MapItemToShowItemViewModel(Item item);
    }
}