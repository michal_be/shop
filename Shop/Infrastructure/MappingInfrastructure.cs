﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using AutoMapper;
using Shop.Dtos;
using Shop.Entities;
using Shop.Models;

namespace Shop.Infrastructure
{
    public class MappingInfrastructure: IMappingInfrastructure
    {
        private readonly IMapper _mapper;

        public MappingInfrastructure(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ManageAccountViewModel MapApplicationUserToManageAccountViewModel(ApplicationUser user)
        {
            ManageAccountViewModel manageAccountViewModel = _mapper.Map<ApplicationUser, ManageAccountViewModel>(user);
            return manageAccountViewModel;
        }

        public Item MapItemFormDtoToItem(ItemFormDto itemFormDto)
        {
            Item item = _mapper.Map<ItemFormDto, Item>(itemFormDto);
            return item;
        }

        public List<ShowItemViewModel> MapItemsToShowItemViewModels(List<Item> items)
        {
            List<ShowItemViewModel> showItemViewModels = _mapper.Map<List<Item>, List<ShowItemViewModel>>(items);
            return showItemViewModels;
        }

        public ApplicationUser MapManageAccountViewModelToApplicationUser(ManageAccountViewModel manageAccountViewModel)
        {
            ApplicationUser user = _mapper.Map<ManageAccountViewModel, ApplicationUser>(manageAccountViewModel);
            return user;
        }

        public ShowItemViewModel MapItemToShowItemViewModel(Item item)
        {
            ShowItemViewModel showItemViewModel = _mapper.Map<Item, ShowItemViewModel>(item);
            return showItemViewModel;
        }

    }
}