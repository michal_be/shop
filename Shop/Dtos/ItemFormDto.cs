﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shop.Entities;
using Shop.Models;

namespace Shop.Dtos
{
    public class ItemFormDto
    {
        public ItemFormDto()
        {
            PhotosList = new List<string>();
            Status = ItemStatus.Active;
        }

        [AllowHtml]
        [WhiteSpaceValidator]
        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "AddItemValidation_NameRequired")]
        [StringLength(100, MinimumLength = 4, ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "AddItemValidation_NameStringLength")]
        [RegularExpression("^[A-Za-z0-9óżźłńąęćśÓŻŹŁŃĄĘĆŚ].*$", ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "AddItemValidation_NameRegularExpression")]
        public string Name { get; set; }


        [AllowHtml]  
        [StringLength(1000, ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "AddItemValidation_DescriptionStringLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "AddItemValidation_PriceRequired")]
        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "AddItemValidation_PriceRegularExpression")]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        public Currency Currency { get; set; }

        public bool Negotiable { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "AddItemValidation_PhotoRequired")]
        [PhotoValidator]
        public List<string> PhotosList { get; set; }

        public Category Category { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "AddItemValidation_CategoryRequired")]
        public byte CategoryId { get; set; }

        public string OwnerId { get; set; }

        public ItemStatus Status { get; set; }
    }
}