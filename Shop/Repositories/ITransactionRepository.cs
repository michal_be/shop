﻿using System.Collections.Generic;
using Shop.Entities;

namespace Shop.Repositories
{
    public interface ITransactionRepository
    {
        void AddTransaction(Transaction transaction);
        int GetAmountOfItemsInChartByUserId(string currentUserId);
        List<Item> GetBoughtItemsList(string userId);
        int GetAmountOfFinalizedTransactions();
    }
}