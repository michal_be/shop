﻿using System.Collections.Generic;
using Shop.Entities;

namespace Shop.Repositories
{
    public interface ICategoryRepository
    {
        List<Category> GetAllowedCategories();
    }
}