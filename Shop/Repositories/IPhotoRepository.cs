﻿using System.Collections.Generic;
using Shop.Entities;
using Shop.Models;

namespace Shop.Repositories
{
    public interface IPhotoRepository
    {
        void InsertPhoto(Photo photo);
        List<Photo> GetPhotosByItemId(long itemId);
        Photo GetPhotoById(long id);
    }
}