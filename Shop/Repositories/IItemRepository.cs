﻿using System.Collections.Generic;
using Shop.Entities;

namespace Shop.Repositories
{
    public interface IItemRepository
    {
        void AddItem(Item item);
        List<Item> GetItemsBySearchQuery(string query);
        Item GetItemById(long itemId);
        void ChangeItemStatus(long itemId, ItemStatus status);
        List<Item> GetItemsByOwnerId(string userId);
        List<Item> GetItemsBySearchQueryAndCategoryId(string query, byte categoryId);
        int GetAmountOfActiveItems();
    }
}