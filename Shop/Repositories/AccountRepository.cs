﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Shop.Models;

namespace Shop.Repositories
{
    public class AccountRepository: IAccountRepository
    {
        private readonly ApplicationDbContext _context;
        
        public AccountRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool CheckIfLoginExists(string login)
        {
            bool loginExists = _context.Users.Any(u => u.Login == login);
            return loginExists;
        }

        public bool CheckIfEmailExists(string email)
        {
            bool emailExists = _context.Users.Any(u => u.Email == email);
            return emailExists;
        }

        public ApplicationUser GetUserById(string userId)
        {
            ApplicationUser user = _context.Users.Single(u => u.Id == userId);
            return user;
        }

        public void UpdateUserAccount(ApplicationUser user)
        {
            _context.Users.Attach(user);
            _context.Entry(user).State = EntityState.Modified;
            _context.SaveChanges();      
        }

        public int GetAmountOfUsers()
        {
            int amountOfUsers = _context.Users.Count();
            return amountOfUsers;
        }
    }
}