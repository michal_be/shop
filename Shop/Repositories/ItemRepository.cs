﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using Shop.Entities;
using Shop.Models;

namespace Shop.Repositories
{
    public class ItemRepository : IItemRepository
    {
        private readonly ApplicationDbContext _context;

        public ItemRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddItem(Item item)
        {
            _context.Items.Add(item);
            _context.SaveChanges();
        }

        public List<Item> GetItemsBySearchQuery(string query)
        {
            List<Item> foundItems = _context.Items.Include(x => x.Photos).Where(x => x.Name.Contains(query) && x.Status == ItemStatus.Active).ToList();
            return foundItems;
        }

        public List<Item> GetItemsBySearchQueryAndCategoryId(string query, byte categoryId)
        {
            List<Item> foundItems = _context.Items.Include(x => x.Photos).Where(x => x.Name.Contains(query) && x.Status == ItemStatus.Active && x.CategoryId == categoryId).ToList();
            return foundItems;
        }

        public Item GetItemById(long itemId)
        {
            Item item = _context.Items.Include(x => x.Owner).SingleOrDefault(x => x.Id == itemId);
            return item;
        }

        public void ChangeItemStatus(long itemId, ItemStatus status)
        {
            Item item = _context.Items.Find(itemId);
            if (item != null)
            {
                item.Status = status;
                _context.Entry(item).CurrentValues.SetValues(item);
                _context.SaveChanges();
            }
        }

        public List<Item> GetItemsByOwnerId(string userId)
        {
            List<Item> items = _context.Transactions
                                       .Where(x => x.BoughtItem.OwnerId == userId && x.Status == TransactionStatus.Chart)
                                       .Select(x => x.BoughtItem).ToList();

            return items;
        }

        public int GetAmountOfActiveItems()
        {
            int amountOfActiveItems = _context.Items.Count(x => x.Status == ItemStatus.Active);
            return amountOfActiveItems;
        }
    }
}