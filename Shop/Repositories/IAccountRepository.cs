﻿using Shop.Models;

namespace Shop.Repositories
{
    public interface IAccountRepository
    {
        bool CheckIfLoginExists(string login);

        bool CheckIfEmailExists(string email);

        ApplicationUser GetUserById(string userId);
        void UpdateUserAccount(ApplicationUser user);
        int GetAmountOfUsers();
    }
}