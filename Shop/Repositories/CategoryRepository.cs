﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Shop.Entities;
using Shop.Models;

namespace Shop.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext _context;

        public CategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Category> GetAllowedCategories()
        {
            List<Category> categories = _context.Categories.Where(c => c.IsDisabled == false).ToList();
            return categories;
        }



    }
}