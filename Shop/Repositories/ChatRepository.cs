﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Shop.Entities;
using Shop.Models;
using Shop.Services;

namespace Shop.Repositories
{
    public class ChatRepository: IChatRepository
    {
        private readonly ApplicationDbContext _context;

        public ChatRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public int? GetConnectionId(string currentUserId, string partnerUserId)
        {
            ChatConnection connection = _context.ChatConnections
                                                 .SingleOrDefault(c =>
                                                                 (c.FirstUserId == currentUserId && c.SecondUserId == partnerUserId) ||
                                                                 (c.FirstUserId == partnerUserId && c.SecondUserId == currentUserId));

            return connection?.ConnectionId;
        }

        public void SaveMessageToDb(ChatMessage chatMessage)
        {
            _context.ChatMessages.Add(chatMessage);
            _context.SaveChanges();
        }

        public int SetConnection(ChatConnection chatConnection)
        {
            _context.ChatConnections.Add(chatConnection);
            _context.SaveChanges();

            return chatConnection.ConnectionId;
        }
    }
}