﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Shop.Entities;
using Shop.Models;

namespace Shop.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private ApplicationDbContext _context;

        public TransactionRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddTransaction(Transaction transaction)
        {
            _context.Transactions.Add(transaction);
            _context.SaveChanges();
        }

        public int GetAmountOfFinalizedTransactions()
        {
            int amountOfFinalizedTransactions = _context.Transactions.Count(x => x.Status == TransactionStatus.Finalized);
            return amountOfFinalizedTransactions;
        }

        public int GetAmountOfItemsInChartByUserId(string userId)
        {
            int amount = _context.Transactions.Count(t => t.BuyerId == userId && t.Status == TransactionStatus.Chart);
            return amount;
        }

        public List<Item> GetBoughtItemsList(string userId)
        {
            List<Item> itemsList = _context.Transactions
                                    .Where(t => t.BuyerId == userId && t.Status == TransactionStatus.Chart)
                                    .Select(t => t.BoughtItem).Include(t => t.Owner).ToList();
            return itemsList;
        }
    }
}