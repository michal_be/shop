﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Shop.Models;
using Shop.Entities;

namespace Shop.Repositories
{
    public class PhotoRepository: IPhotoRepository
    {
        private ApplicationDbContext _context;

        public PhotoRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void InsertPhoto(Photo photo)
        {
            _context.Photos.Add(photo);
            _context.SaveChanges();

        }

        public List<Photo> GetPhotosByItemId(long itemId)
        {
            List<Photo> photosList = _context.Photos.Where(p => p.ItemId == itemId).ToList();
            return photosList;
        }

        public Photo GetPhotoById(long id)
        {
            Photo photo = _context.Photos.SingleOrDefault(p => p.Id == id);
            return photo;
        }
    }
}