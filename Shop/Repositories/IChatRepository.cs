﻿using Shop.Entities;

namespace Shop.Repositories
{
    public interface IChatRepository
    {
        int SetConnection(ChatConnection chatConnection);
        int? GetConnectionId(string currentUserId, string partnerUserId);
        void SaveMessageToDb(ChatMessage chatMessage);
    }
}