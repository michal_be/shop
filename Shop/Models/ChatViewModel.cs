﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class ChatViewModel
    {
        public int ConnectionId { get; set; }
        public ApplicationUser PartnerUser { get; set; }

    }
}