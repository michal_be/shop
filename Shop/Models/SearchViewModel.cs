﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shop.Models
{
    public class SearchViewModel
    {
        public SelectList Categories { get; set; }

        public SearchViewModel()
        {
            Categories = new SelectList(new[]
            {
                new SelectListItem {Value = "1", Text = "Elektronika"},
                new SelectListItem {Value = "2", Text = "Moda i uroda"},
                new SelectListItem {Value = "3", Text = "Dom i zdrowie"},
                new SelectListItem {Value = "4", Text = "Dziecko"},
                new SelectListItem {Value = "5", Text = "Kultura i rozrywka"},
                new SelectListItem {Value = "6", Text = "Elektronika"},
                new SelectListItem {Value = "7", Text = "Sport i wypoczynek"},
                new SelectListItem {Value = "8", Text = "Motoryzacja"},
                new SelectListItem {Value = "9", Text = "Kolekcje i sztuka"},
                new SelectListItem {Value = "10", Text = "Firma"},
                new SelectListItem {Value = "11", Text = "Inne"}
            }, "Value", "Text");
        }    
    }
}