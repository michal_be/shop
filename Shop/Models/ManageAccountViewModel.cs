﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class ManageAccountViewModel
    {
        public string Name { get; set; } 
        public string Surname { get; set; }
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_EmailRequired")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_EmailRegularExpression")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_BirthdayRequired")]
        public string BirthDate { get; set; }

    }
}