﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Shop.Resources;

namespace Shop.Models
{
    public class WhiteSpaceValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                string valueString = value.ToString();

                if (valueString.Trim().Length == 0)
                {
                    return new ValidationResult(ResourcesPolish.Validation_WhiteSpace);
                }
                return ValidationResult.Success;

            }

            return null;
        }
    }
}