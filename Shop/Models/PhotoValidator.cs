﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Shop.Resources;

namespace Shop.Models
{
    public class PhotoValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var photosNames = value as List<string>;
            if (photosNames != null)
            {
                foreach (var photoName in photosNames)
                {
                    if (photoName != "")
                    {
                        return ValidationResult.Success;
                    }
                }               
            }
            return new ValidationResult(ResourcesPolish.AddItemValidation_PhotoRequired);
        }
    }
}