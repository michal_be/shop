﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;
using Shop.Resources;

namespace Shop.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
       
        [Display(Name = "Login")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType =typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "LoginFormValidation_PasswordRequired")]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Zapamiętaj mnie?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {

        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_NameRequired")]
        [StringLength(20,ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_NameRegularExpression")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_SurnameRequired")]
        [StringLength(20, ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_SurnameRegularExpression")]
        public string Surname { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_LoginRequired")]
        [RegularExpression(@"^[A-Za-z0-9_-].*$", ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_LoginRegularExpression")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_EmailRequired")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_EmailRegularExpression")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_BirthdayRequired")]  
        public string BirthDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_PasswordRequired")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_PasswordRegularExpression", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.ResourcesPolish), ErrorMessageResourceName = "RegisterFormValidation_PasswordRepeat")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
