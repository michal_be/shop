﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shop.Dtos;
using Shop.Entities;

namespace Shop.Models
{
    public class ItemFormViewModel
    {

        public ItemFormDto ItemFormDto { get; set; }
        public SelectList Categories { get; set; }
        public SelectList Currencies { get; set; }

        public ItemFormViewModel(ItemFormDto itemFormDto, IEnumerable<Category> categories)
        {
            ItemFormDto = itemFormDto;
            Categories = new SelectList(categories.Where(c => c.IsDisabled == false),"Id", "Name");
            Currencies = new SelectList(Enum.GetValues(typeof(Currency)));
        }
    }
}