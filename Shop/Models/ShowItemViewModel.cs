﻿using Shop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class ShowItemViewModel
    {
        public ShowItemViewModel()
        {
            Photos = new List<Photo>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Currency Currency { get; set; }
        public bool Negotiable { get; set; }
        public List<Photo> Photos { get; set; }
        public Category Category { get; set; }
        public byte CategoryId { get; set; }

        public string MainPhotoPath
        {
            get { return $"/api/photo/{Photos.First().Id}"; }
            
        }

        public string OwnerId { get; set; }

        public ItemStatus Status { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public string PhotoPath(long id)
        {
            return $"/api/photo/{id}"; 
        }
    }
}