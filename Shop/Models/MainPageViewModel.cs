﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class MainPageViewModel
    {
        public int AmountOfUsers { get; set; }
        public int AmountOfActiveOffers { get; set; }
        public int AmountOfFinalizedOffers { get; set; }
    }
}