﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Shop.Models;
using Shop.Services;

namespace Shop.Controllers
{
    [Authorize]
    public class ChatController : Controller
    {
        private readonly IChatService _chatService;
        public ChatController(IChatService chatService)
        {
            _chatService = chatService;
        }
        public ActionResult ShowChat(string partnerUserId)
        {
            string currentUserId = HttpContext.User.Identity.GetUserId();
            int connectionId = _chatService.SetConnection(currentUserId, partnerUserId);
            ChatViewModel chatViewModel = _chatService.GetChatViewModel(connectionId, partnerUserId);

            return View(chatViewModel);
        }
    }
}