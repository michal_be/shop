﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Shop.Entities;
using Shop.Services;
using Shop.Models;

namespace Shop.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IPhotoService _photoService;

        public HomeController(IAccountService accountService, IPhotoService photoService)
        {
            _accountService = accountService;
            _photoService = photoService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult MyAccount()
        {
            string userId = User.Identity.GetUserId();
            ManageAccountViewModel manageAccountViewModel = _accountService.GetManageAccountViewModelByUserId(userId);
            return View(manageAccountViewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult MyAccount(ManageAccountViewModel manageAccountViewModel)
        {
            
            if (!ModelState.IsValid)
            {
                return View(manageAccountViewModel);
            }

            _accountService.UpdateUserAccount(manageAccountViewModel);
            return RedirectToAction("Index", "Home");
        }


        //Below code is fake just to try get image displayed properly

        public ActionResult GetPhoto()
        {
            var itemId = 1;
            var photosPaths = _photoService.GetPhotosPathsByItemId(itemId);
            byte[] file = System.IO.File.ReadAllBytes(photosPaths.First());
            var modelToView = new PhotoViewModel();
            modelToView.File = file;
            return View(modelToView);
        }


     

    }
}