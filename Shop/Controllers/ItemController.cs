﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Shop.Dtos;
using Shop.Entities;
using Shop.Models;
using Shop.Services;
using X.PagedList;

namespace Shop.Controllers
{
    [Authorize]
    public class ItemController : Controller
    {
        private readonly IItemService _itemService;
        private readonly IPhotoService _photoService;
        private readonly ITransactionService _transactionService;

        public ItemController(IItemService itemService, IPhotoService photoService, ITransactionService transactionService)
        {
            _itemService = itemService;
            _photoService = photoService;
            _transactionService = transactionService;
        }
       
        public ActionResult AddItem()
        {
            ItemFormViewModel viewModel = _itemService.GetItemFormViewModel();
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddItem([Bind(Prefix = "ItemFormDto")] ItemFormDto itemFormDto)
        {    
            if (!ModelState.IsValid)
            {
                ItemFormViewModel viewModel = _itemService.GetItemFormViewModel();
                viewModel.ItemFormDto = itemFormDto;
                return View(viewModel);
            }

            long itemId = _itemService.AddItemAndReturnId(itemFormDto);
            _photoService.UploadPhotos(itemId, itemFormDto.PhotosList);

            return RedirectToAction("Index", "Home");
        }


        [HttpPost]
        public JsonResult UploadPhotoToTempDirectory()
        {
            HttpPostedFileBase fileUpload = Request.Files[0];   
            if (fileUpload != null && fileUpload.ContentLength > 0)
            {
                var photoName = Guid.NewGuid() + "_" + fileUpload.FileName;
                fileUpload.SaveAs(_photoService.GetTempImagePath(photoName));
                return Json(new { success = photoName });
            }
            return Json(new { success = "" });
        }

        public ActionResult SearchItem(string query, byte categoryId = 0, int pageIndex = 1)
        {
            IPagedList<ShowItemViewModel> foundItemsViewModel = _itemService.GetItemsBySearchQueryAndCategoryId(query, categoryId, pageIndex);
            return View(foundItemsViewModel);
        }

        public ActionResult ShowItemOffer(long itemId)
        {
            ShowItemViewModel showItemViewModel = _itemService.GetShowItemViewModel(itemId);
            return View(showItemViewModel);
        }

        public ActionResult BuyItem(long itemId)
        {
            _itemService.AddItemToCart(itemId);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ShowCart()
        {
            string currentUserId = HttpContext.User.Identity.GetUserId();
            List<ShowItemViewModel> showItemsViewModel = _transactionService.GetBoughItemsByUser(currentUserId);
            return View(showItemsViewModel);
        }

        public ActionResult MyOffers()
        {
            string currentUserId = HttpContext.User.Identity.GetUserId();
            List<ShowItemViewModel> showItemsViewModel = _itemService.GetItemOffersByOwnerId(currentUserId);
            return View(showItemsViewModel);
        }

        //public ActionResult FinalizeAuction(long itemId)
        //{
        //    _itemService.ChangeItemStatusToBought(itemId);
        //    _transactionService.ChangeTransactionStatusToFinalized(itemId);

        //}




    }
}