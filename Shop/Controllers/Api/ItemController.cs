﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using Shop.Services;

namespace Shop.Controllers.Api
{
    public class ItemController : ApiController
    {
        private readonly ITransactionService _transactionService;

        public ItemController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [Route("api/item/getAmountOfItemsInChart")]
        [HttpGet]
        public JsonResult<int> GetAmountOfItemsInChart()
        {
            var loggedIn = HttpContext.Current.User.Identity.IsAuthenticated;
            if (loggedIn == false)
                return Json(0);

            var currentUserId = HttpContext.Current.User.Identity.GetUserId();
            int amountOfItems = _transactionService.GetAmountOfItemsInChartByUserId(currentUserId);

            return Json(amountOfItems);
        } 

    }
}
