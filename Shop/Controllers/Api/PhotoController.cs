﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Shop.Services;

namespace Shop.Controllers.Api
{
    [System.Web.Mvc.Authorize]
    public class PhotoController : ApiController
    {
        private readonly IPhotoService _photoService;

        public PhotoController(IPhotoService photoService)
        {
            _photoService = photoService;
        }

        public HttpResponseMessage Get(long id)
        {
            byte[] photo = _photoService.GetPhotoData(id);
            HttpResponseMessage response = new HttpResponseMessage();

            if (photo == null)
            {
               response.StatusCode = HttpStatusCode.NotFound;
               return response;
            }

            response.StatusCode = HttpStatusCode.OK;
            response.Content = new ByteArrayContent(photo);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
            return response;
        }

    }
}
