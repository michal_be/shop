﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web.Http;
using Shop.Models;
using Shop.Services;

namespace Shop.Controllers.Api
{
    public class MainPageController : ApiController
    {
        private readonly IAccountService _accountService;
        private readonly IItemService _itemService;

        public MainPageController(IAccountService accountService, IItemService itemService)
        {
            _accountService = accountService;
            _itemService = itemService;
        }

        [Route("api/mainPage")]
        [HttpGet]
        public IHttpActionResult GetMainPageData()
        {
            MainPageViewModel mainPageViewModel = _itemService.GetMainPageViewModel();
            return Ok(mainPageViewModel);
        }
    }
}
