﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Shop.Services;

namespace Shop.Controllers.Api
{
    
    public class AccountController : ApiController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountSerive)
        {
            _accountService = accountSerive;
        }


        [Route("api/account/checkIfLoginExists")]
        [HttpGet]
        public JsonResult<bool> CheckIfLoginExists(string login)
        {
            bool loginExists = _accountService.CheckIfLoginExists(login);
            return Json(loginExists);
        }

        [Route("api/account/checkIfEmailExists")]
        [HttpGet]
        public JsonResult<bool> CheckIfEmailExists(string email)
        {
            bool emailExists = _accountService.CheckIfEmailExists(email);
            return Json(emailExists);
        } 

    }
}
