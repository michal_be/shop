﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Shop.Entities;
using Shop.Infrastructure;
using Shop.Models;
using Shop.Repositories;

namespace Shop.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly IMappingInfrastructure _mappingInfrastructure;

        public TransactionService(ITransactionRepository transactionRepository, IMappingInfrastructure mappingInfrastructure)
        {
            _transactionRepository = transactionRepository;
            _mappingInfrastructure = mappingInfrastructure;
        }

        public int GetAmountOfItemsInChartByUserId(string currentUserId)
        {
            var amountOfItems = _transactionRepository.GetAmountOfItemsInChartByUserId(currentUserId);
            return amountOfItems;
        }

        public List<ShowItemViewModel> GetBoughItemsByUser(string userId)
        {
            List<Item> boughtItems = _transactionRepository.GetBoughtItemsList(userId);
            List<ShowItemViewModel> boughtItemsViewModel = _mappingInfrastructure.MapItemsToShowItemViewModels(boughtItems);

            return boughtItemsViewModel;
        }
    }
}