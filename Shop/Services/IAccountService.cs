﻿using Shop.Models;

namespace Shop.Services
{
    public interface IAccountService
    {
         bool CheckIfLoginExists(string login);
         bool CheckIfEmailExists(string email);
         ManageAccountViewModel GetManageAccountViewModelByUserId(string userId);
         void UpdateUserAccount(ManageAccountViewModel manageAccountViewModel);
    }
}