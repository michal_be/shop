﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using Shop.Entities;
using Shop.Repositories;

namespace Shop.Services
{
    public class PhotoService: IPhotoService
    {
        private readonly IPhotoRepository _photoRepository;

        public PhotoService(IPhotoRepository photoRepository)
        {
            _photoRepository = photoRepository;
        }

        public void InsertPhotoNameToDb(long itemId, string photoName)
        {
            var photo = new Photo
            {
                ItemId = itemId,
                Name = photoName
            };
            _photoRepository.InsertPhoto(photo);
        }

        public string GetTempImagePath(string fileName)
        {
            var tempPathString = System.Configuration.ConfigurationManager.AppSettings["tempImageDirectoryPath"];
            return $"{tempPathString}\\{fileName}";
        }

        public void UploadPhotos(long itemId, List<string> photosList)
        {
            foreach (var photoName in photosList)
            {
                if (photoName != "")
                {
                    MovePhotoToTargetDirectory(photoName);
                    InsertPhotoNameToDb(itemId, photoName);
                }
            }
        }

        private void MovePhotoToTargetDirectory(string photoName)
        {
            var tempImageDirectoryPath = GetTempImagePath(photoName);
            var imageDirectoryPath = GetImagePath(photoName);

            File.Copy(tempImageDirectoryPath, imageDirectoryPath);
            File.Delete(tempImageDirectoryPath);
        }

        private string GetImagePath(string fileName)
        {
            var pathString = System.Configuration.ConfigurationManager.AppSettings["imageDirectoryPath"];
            return $"{pathString}\\{fileName}";
        }

        public List<string> GetPhotosPathsByItemId(long itemId)
        {
            List<Photo> photosList = _photoRepository.GetPhotosByItemId(itemId);
            List<string> imagePaths = new List<string>();

            foreach (var photo in photosList)
            {
                string photoPath = GetImagePath(photo.Name);
                imagePaths.Add(photoPath);
            }

            return imagePaths;
        }

        public byte[] GetPhotoData(long id)
        {
            var photo = _photoRepository.GetPhotoById(id);
            string photoPath = GetImagePath(photo.Name);
            byte[] photoBytes= File.ReadAllBytes(photoPath);

            return photoBytes;
        }
    }
}