﻿using System.Collections.Generic;
using Shop.Dtos;
using Shop.Models;
using X.PagedList;

namespace Shop.Services
{
    public interface IItemService
    {
        ItemFormViewModel GetItemFormViewModel();
        long AddItemAndReturnId(ItemFormDto itemFormDto);
        ShowItemViewModel GetShowItemViewModel(long itemId);
        void AddItemToCart(long itemId);
        List<ShowItemViewModel> GetItemOffersByOwnerId(string userId);
        IPagedList<ShowItemViewModel> GetItemsBySearchQueryAndCategoryId(string query, byte categoryId, int pageIndex);
        MainPageViewModel GetMainPageViewModel();
    }
}