﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Shop.Entities;
using Shop.Models;
using Shop.Repositories;

namespace Shop.Services
{
    public class ChatService: IChatService
    {
        private readonly IChatRepository _chatRepository;
        private readonly IAccountRepository _accountRepository;

        public ChatService(IChatRepository chatRepository, IAccountRepository accountRepository)
        {
            _chatRepository = chatRepository;
            _accountRepository = accountRepository;
        }

        public ChatViewModel GetChatViewModel(int connectionId, string partnerUserId)
        {
            ApplicationUser partnerUser = _accountRepository.GetUserById(partnerUserId);

            ChatViewModel chatViewModel = new ChatViewModel()
            {
                ConnectionId = connectionId,
                PartnerUser = partnerUser
            };

            return chatViewModel;
        }

        public int SetConnection(string currentUserId, string partnerUserId)
        {
            int? connectionId = _chatRepository.GetConnectionId(currentUserId, partnerUserId);

            if (connectionId != null)
            {
                return (int)connectionId;
            }

            ChatConnection chatConnection = new ChatConnection()
            {
                FirstUserId = currentUserId,
                SecondUserId = partnerUserId
            };
            int newConnectionId = _chatRepository.SetConnection(chatConnection);

            return newConnectionId;
        }

        public void SaveMessageToDb(string message, int connectionId, string authorMessageId)
        {

            ChatMessage chatMessage = new ChatMessage()
            {
                ConnectionId = connectionId,
                MessageAuthorId = authorMessageId,
                MessageContent = message,
                MessageDate = DateTime.Now
            };

            _chatRepository.SaveMessageToDb(chatMessage);
        }
    }
}