﻿using System.Collections.Generic;
using Shop.Models;

namespace Shop.Services
{
    public interface ITransactionService
    {
        int GetAmountOfItemsInChartByUserId(string currentUserId);
        List<ShowItemViewModel> GetBoughItemsByUser(string userId);
    }
}