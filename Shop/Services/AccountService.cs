﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Shop.Infrastructure;
using Shop.Models;
using Shop.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Shop.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IMappingInfrastructure _mappingInfrastructure;

        public AccountService(IAccountRepository accountRepository, IMappingInfrastructure mappingInfrastructure)
        {
            _accountRepository = accountRepository;
            _mappingInfrastructure = mappingInfrastructure;
        }

        public bool CheckIfEmailExists(string email)
        {
            bool emailExists = _accountRepository.CheckIfEmailExists(email);
            return emailExists;
        }

        public bool CheckIfLoginExists(string login)
        {
            bool loginExists = _accountRepository.CheckIfLoginExists(login);
            return loginExists;
        }

        public ManageAccountViewModel GetManageAccountViewModelByUserId(string userId)
        {
            ApplicationUser user = _accountRepository.GetUserById(userId);
            ManageAccountViewModel manageAccountViewModel = _mappingInfrastructure.MapApplicationUserToManageAccountViewModel(user);
            return manageAccountViewModel;
        }

        public void UpdateUserAccount(ManageAccountViewModel manageAccountViewModel)
        {
            ApplicationUser user = _mappingInfrastructure.MapManageAccountViewModelToApplicationUser(manageAccountViewModel);
            DateTime userBirthday = DateTime.ParseExact(manageAccountViewModel.BirthDate, "dd-MM-yyyy", null);
            user.BirthDate = userBirthday;    
            user.Id = HttpContext.Current.User.Identity.GetUserId();
            _accountRepository.UpdateUserAccount(user);
        }

    }
}