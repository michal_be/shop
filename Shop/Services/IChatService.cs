﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Models;

namespace Shop.Services
{
    public interface IChatService
    {
       int SetConnection(string currentUserId, string partnerUserId);
       ChatViewModel GetChatViewModel(int connectionId, string partnerUserId);
        void SaveMessageToDb(string message, int connectionId, string authorMessageId);
    }
}
