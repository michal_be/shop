﻿using System.Collections.Generic;
using Shop.Entities;

namespace Shop.Services
{
    public interface IPhotoService
    {
         string GetTempImagePath(string fileName);
         void UploadPhotos(long itemId, List<string> photosList);
         List<string> GetPhotosPathsByItemId(long itemId);
         byte[] GetPhotoData(long id);
    }
}