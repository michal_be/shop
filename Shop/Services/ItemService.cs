﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Shop.Dtos;
using Shop.Entities;
using Shop.Infrastructure;
using Shop.Models;
using Shop.Repositories;
using X.PagedList;

namespace Shop.Services
{
    public class ItemService : IItemService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IMappingInfrastructure _mappingInfrastructure;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IAccountRepository _accountRepository;

        public ItemService(ICategoryRepository categoryRepository, IItemRepository itemRepository, IMappingInfrastructure mappingInfrastructure, ITransactionRepository transactionRepository, IAccountRepository accountRepository)
        {
            _categoryRepository = categoryRepository;
            _itemRepository = itemRepository;
            _mappingInfrastructure = mappingInfrastructure;
            _transactionRepository = transactionRepository;
            _accountRepository = accountRepository;
        }

        public long AddItemAndReturnId(ItemFormDto itemFormDto)
        {
            Item item = _mappingInfrastructure.MapItemFormDtoToItem(itemFormDto);
            item.OwnerId = HttpContext.Current.User.Identity.GetUserId();
            _itemRepository.AddItem(item);

            return item.Id;
        }

        public void AddItemToCart(long itemId)
        {
            string currentUser = HttpContext.Current.User.Identity.GetUserId();
            Transaction transaction = new Transaction()
            {
                BuyerId = currentUser,
                ItemId = itemId,
                Status = TransactionStatus.Chart
            };
            _transactionRepository.AddTransaction(transaction);
            _itemRepository.ChangeItemStatus(itemId, ItemStatus.Bought);

        }
     
        public ItemFormViewModel GetItemFormViewModel()
        {
            List<Category> categories = _categoryRepository.GetAllowedCategories();
            ItemFormViewModel itemFormViewModel = new ItemFormViewModel(new ItemFormDto(), categories);

            return itemFormViewModel;
        }

        public List<ShowItemViewModel> GetItemOffersByOwnerId(string userId)
        {
            List<Item> items = _itemRepository.GetItemsByOwnerId(userId);
            List<ShowItemViewModel> itemOffersViewModel = _mappingInfrastructure.MapItemsToShowItemViewModels(items);

            return itemOffersViewModel;
        }

        public IPagedList<ShowItemViewModel> GetItemsBySearchQueryAndCategoryId(string query, byte categoryId, int pageIndex)
        {
            List<Item> foundItems = new List<Item>();
            if (categoryId!=0)
            {                               
                foundItems = _itemRepository.GetItemsBySearchQueryAndCategoryId(query, categoryId);
            }
            else
            {
                foundItems = _itemRepository.GetItemsBySearchQuery(query);
            }
            List<ShowItemViewModel> foundItemsViewModel = _mappingInfrastructure.MapItemsToShowItemViewModels(foundItems);
            var amountOfItemsPerPage = Convert.ToInt32(ConfigurationManager.AppSettings["amountOfItemsPerPage"]);
            var pagedList = foundItemsViewModel.ToPagedList(pageIndex, amountOfItemsPerPage);
            return pagedList;
        }

        public MainPageViewModel GetMainPageViewModel()
        {
            MainPageViewModel mainPageViewModel = new MainPageViewModel()
            {
                AmountOfActiveOffers = _itemRepository.GetAmountOfActiveItems(),
                AmountOfFinalizedOffers = _transactionRepository.GetAmountOfFinalizedTransactions(),
                AmountOfUsers = _accountRepository.GetAmountOfUsers()
            };
    
            return mainPageViewModel;
        }

        public ShowItemViewModel GetShowItemViewModel(long itemId)
        {
            Item item = _itemRepository.GetItemById(itemId);
            ShowItemViewModel showItemViewModel = _mappingInfrastructure.MapItemToShowItemViewModel(item);

            return showItemViewModel;
        }
    }
}