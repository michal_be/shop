using System;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Shop.Controllers;
using Shop.Infrastructure;
using Shop.Models;
using Shop.Repositories;
using Shop.Services;

namespace Shop.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            // container.RegisterType<IProductRepository, ProductRepository>();

            container.RegisterType<UserManager<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>();
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());

            container.RegisterType<IItemRepository, ItemRepository>();
            container.RegisterType<ICategoryRepository, CategoryRepository>();
            container.RegisterType<IPhotoRepository, PhotoRepository>();
            container.RegisterType<IAccountRepository, AccountRepository>();
            container.RegisterType<ITransactionRepository, TransactionRepository>();
            container.RegisterType<IChatRepository, ChatRepository>();

            container.RegisterType<IItemService,ItemService>();        
            container.RegisterType<IPhotoService,PhotoService>();
            container.RegisterType<IAccountService, AccountService>();
            container.RegisterType<ITransactionService, TransactionService>();
            container.RegisterType<IChatService, ChatService>();
            
            container.RegisterType<IMappingInfrastructure, MappingInfrastructure>();
            container.RegisterType<IMapper>(new InjectionFactory(c => AutoMapperConfig.ConfigureMapper()));

        }
    }
}
