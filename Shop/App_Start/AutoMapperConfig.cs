﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AutoMapper;
using Shop.Dtos;
using Shop.Entities;
using Shop.Models;

namespace Shop.App_Start
{
    public static class AutoMapperConfig
    {
        public static IMapper ConfigureMapper()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ItemFormDto, Item>();
                cfg.CreateMap<ApplicationUser, ManageAccountViewModel>();
                cfg.CreateMap<ManageAccountViewModel, ApplicationUser>().ForMember(x => x.BirthDate, opt => opt.Ignore())
                                                                        .ForMember(dest => dest.Login, opts => opts.MapFrom(src => src.UserName));
                cfg.CreateMap<Item, ShowItemViewModel>()
                    .ForMember(dest => dest.Login, opts => opts.MapFrom(src => src.Owner.Login))
                    .ForMember(dest => dest.Email, opts => opts.MapFrom(src => src.Owner.Email));

            });

            var mapper = configuration.CreateMapper();
            return mapper;
        }
    }
}