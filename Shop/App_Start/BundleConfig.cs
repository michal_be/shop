﻿using System.Web;
using System.Web.Optimization;

namespace Shop
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/*.css"));

            bundles.Add(new ScriptBundle("~/bundles/dropzone").Include(
                     "~/Scripts/dropzone.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                     "~/Scripts/knockout-3.4.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/combodate").Include(
                   "~/Scripts/combodate.js",
                   "~/Scripts/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/registerValidation").Include(
                    "~/Scripts/registerValidation.js"));

            bundles.Add(new ScriptBundle("~/bundles/myAccountValidation").Include(
                    "~/Scripts/myAccountValidation.js"));

            bundles.Add(new ScriptBundle("~/bundles/mainPage").Include(
                "~/Scripts/mainPage.js"));
        }
    }
}
